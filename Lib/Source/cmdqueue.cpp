#include "../Include/cmdqueue.h"

#include <vector>
#include <iostream>

using namespace std;

NS_CLPP_BEGIN

CommandQueue * CommandQueue::BuildCommandQueue(Context * context)
{
	auto deviceIDS = context->GetContextDevices();
	TEST_GIVEN_EXPRESSION_AND_THROW_CLPP_EXCEPTION(deviceIDS.size() == 0, "创建命令队列时发现上下文设备错误");

	cl_int errNum;
	cl_command_queue cmdQueueObject = clCreateCommandQueue(context->GetRaw(), deviceIDS[0], 0, &errNum);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "创建命令队列失败");

	return new CommandQueue(cmdQueueObject);
}

CommandQueue::CommandQueue(cl_command_queue cmdQueueObject) :
	m_CommandQueueObject(cmdQueueObject)
{
}

CommandQueue::~CommandQueue()
{
	cout << "delete command queue" << endl;
}

void CommandQueue::AddRef()
{
	::clRetainCommandQueue(this->m_CommandQueueObject);
}

void CommandQueue::Release()
{
	auto refCount = this->RefCount();
	::clReleaseCommandQueue(this->m_CommandQueueObject);
	if (refCount == 1)
	{
		delete this;
	}
}

unsigned long CommandQueue::RefCount()
{
	shared_ptr<void> data;
	size_t size;

	this->QueueCommandQueueParamData(CL_QUEUE_REFERENCE_COUNT, data, size);

	cl_uint refCount = *(cl_uint *)data.get();

	return refCount;
}

cl_command_queue CommandQueue::GetRaw()
{
	return this->m_CommandQueueObject;
}

void CommandQueue::QueueCommandQueueParamData(cl_command_queue_info cmdQueueInfo, shared_ptr<void> & data, size_t & size)
{
	cl_int errNum;

	errNum = clGetCommandQueueInfo(this->m_CommandQueueObject, cmdQueueInfo, 0, nullptr, &size);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "查询命令队列属性尺寸失败");

	data.reset(new char[size]);

	errNum = clGetCommandQueueInfo(this->m_CommandQueueObject, cmdQueueInfo, size, data.get(), nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "查询命令队列属性失败");
}

NS_CLPP_END