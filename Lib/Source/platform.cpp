#pragma once

#include "../Include/platform.h"
#include "../Include/exception.h"

using namespace std;

NS_CLPP_BEGIN

vector<cl_platform_id> Platform::EnumAllPlatformID()
{
	cl_int error;
	cl_uint idcount;

	error = ::clGetPlatformIDs(0, nullptr, &idcount);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "查询CL平台ID时发生异常");
	TEST_GIVEN_EXPRESSION_AND_THROW_CLPP_EXCEPTION(idcount < 1, "没有找到任何可用的平台");

	unique_ptr<cl_platform_id> ids(new cl_platform_id[idcount]);

	error = ::clGetPlatformIDs(idcount, ids.get(), nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "查询CL平台ID时发生异常");

	vector<cl_platform_id> vid;
	for (int i = 0; i < idcount; i++)
	{
		vid.push_back(ids.get()[i]);
	}

	return vid;
}

shared_ptr<Platform> Platform::BuildPlatform(cl_platform_id platformID)
{
	return make_shared<Platform>(platformID);
}


Platform::Platform(cl_platform_id platformID) :
	m_PlatformID(platformID)
{}


string Platform::GetProfile()
{
	return this->QueryPlatformInfo(CL_PLATFORM_PROFILE);
}

string Platform::GetVersion()
{
	return this->QueryPlatformInfo(CL_PLATFORM_VERSION);
}

string Platform::GetName()
{
	return this->QueryPlatformInfo(CL_PLATFORM_NAME);
}

string Platform::GetVendor()
{
	return this->QueryPlatformInfo(CL_PLATFORM_VENDOR);
}

string Platform::GetExtension()
{
	return this->QueryPlatformInfo(CL_PLATFORM_EXTENSIONS);
}

cl_platform_id Platform::GetPlatformID()
{
	return this->m_PlatformID;
}

string Platform::QueryPlatformInfo(cl_platform_info name)
{
	cl_int error;
	size_t size;

	error = ::clGetPlatformInfo(
		this->m_PlatformID,
		name,
		0,
		nullptr,
		&size);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "clGetPlatformInfo运行异常");

	unique_ptr<char> value(new char[size]);

	error = ::clGetPlatformInfo(
		this->m_PlatformID,
		name,
		size,
		value.get(),
		nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "clGetPlatformInfo运行异常");

	string csValue(value.get());
	return csValue;
}

NS_CLPP_END