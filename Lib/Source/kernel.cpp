#include "../Include/kernel.h"

#include <iostream>

NS_CLPP_BEGIN

Kernel::Kernel(cl_kernel kernel) :
	m_KernelObject(kernel)
{
}

Kernel::~Kernel()
{
	cout << "delete kernel" << endl;
}

void Kernel::AddRef()
{
	::clRetainKernel(this->m_KernelObject);
}

void Kernel::Release()
{
	auto refCount = this->RefCount();
	::clReleaseKernel(this->m_KernelObject);
	if (refCount == 1)
	{
		delete this;
	}
}

unsigned long Kernel::RefCount()
{
	shared_ptr<void> data;
	size_t size;

	this->QueryKernelObjectData(CL_KERNEL_REFERENCE_COUNT, data, size);

	cl_uint refCount = *(cl_uint *)data.get();

	return refCount;
}

template<class T>
void Kernel::SetBasicTypeArgument(int argIndex, T * argValue)
{
	cl_int errNum = ::clSetKernelArg(this->m_KernelObject, argIndex, sizeof(T), (void *)argValue);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "设置基本类型的内核参数失败");
}

void Kernel::SetBufferArgument(int argIndex, Buffer * mem)
{
	cl_mem bufferRawObj = mem->GetRaw();

	cl_int errNum = ::clSetKernelArg(this->m_KernelObject, argIndex, sizeof(cl_mem), (void *)&bufferRawObj);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "设置缓冲型的内核参数失败");
}

cl_kernel Kernel::GetRaw()
{
	return this->m_KernelObject;
}

void Kernel::QueryKernelObjectData(cl_kernel_info name, shared_ptr<void> & data, size_t & size)
{
	cl_int errNum;

	errNum = ::clGetKernelInfo(this->m_KernelObject, name, 0, nullptr, &size);

	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "查询内核对象属性尺寸失败");

	data.reset(new char[size]);

	errNum = ::clGetKernelInfo(this->m_KernelObject, name, size, data.get(), nullptr);

	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "查询内核对象属性失败");
}

NS_CLPP_END