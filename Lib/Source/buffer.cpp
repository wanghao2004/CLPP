#include "../Include/buffer.h"

NS_CLPP_BEGIN

Buffer * Buffer::BuildBufferObject(Context * context, cl_mem_flags flags, void * data, size_t size)
{
	shared_ptr<void> hostMemory(new char[size]);

	cl_int errNum;

	::memcpy(hostMemory.get(), data, size);

	cl_mem memObject = ::clCreateBuffer(context->GetRaw(), flags, size, hostMemory.get(), &errNum);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "创建内存对象失败");

	return new Buffer(memObject, hostMemory, size, flags);
}

Buffer::Buffer(cl_mem clBufferObj, shared_ptr<void> data, size_t size, cl_mem_flags flages) :
	m_BufferObject(clBufferObj),
	m_Data(data),
	m_Size(size),
	m_Flags(flages)
{
}

Buffer::~Buffer()
{
	cout << "delete memory object" << endl;
}

void Buffer::AddRef()
{
	::clRetainMemObject(this->m_BufferObject);
}

void Buffer::Release()
{
	auto refCount = this->RefCount();
	::clReleaseMemObject(this->m_BufferObject);
	if (refCount == 1)
	{
		delete this;
	}
}

unsigned long Buffer::RefCount()
{
	shared_ptr<void> data;
	size_t size;

	this->QueryBufferParamData(CL_MEM_REFERENCE_COUNT, data, size);

	cl_uint refCount = *(cl_uint *)(data.get());

	return refCount;
}

void * Buffer::GetHostBufferPtr()
{
	return this->m_Data.get();
}

size_t Buffer::GetHostBufferSize()
{
	return this->m_Size;
}

cl_mem_flags Buffer::GetBufferFlags()
{
	return this->m_Flags;
}

cl_mem Buffer::GetRaw()
{
	return this->m_BufferObject;
}

void Buffer::QueryBufferParamData(cl_mem_info name, shared_ptr<void> & data, size_t & size)
{
	cl_int errNum;

	errNum = ::clGetMemObjectInfo(this->m_BufferObject, name, 0, nullptr, &size);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "查询内存对象属性尺寸失败");

	data.reset(new char[size]);

	errNum = ::clGetMemObjectInfo(this->m_BufferObject, name, size, data.get(), nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "查询内存对象属性尺寸失败");

}

NS_CLPP_END