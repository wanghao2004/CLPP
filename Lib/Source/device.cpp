#pragma once

#include "../Include/device.h"

using namespace std;

NS_CLPP_BEGIN

vector<cl_device_id> Device::EnumGPUDevcies(const shared_ptr<Platform> & platform)
{
	cl_int error;
	cl_uint size;

	error = ::clGetDeviceIDs(
		platform->GetPlatformID(),
		CL_DEVICE_TYPE_ALL,
		0,
		nullptr,
		&size);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "获取设备ID发生异常");
	TEST_GIVEN_EXPRESSION_AND_THROW_CLPP_EXCEPTION(size < 1, "没有从平台中找到任何设备");

	unique_ptr<cl_device_id> ids(new cl_device_id[size]);

	error = ::clGetDeviceIDs(
		platform->GetPlatformID(),
		CL_DEVICE_TYPE_ALL,
		size,
		ids.get(),
		nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "获取设备ID发生异常");

	vector<cl_device_id> vid;
	for (int i = 0; i < size; i++)
	{
		vid.push_back(ids.get()[i]);
	}

	return vid;
}

shared_ptr<Device> Device::BuildDeviceByID(cl_platform_id pid, cl_device_id did)
{
	return make_shared<Device>(pid, did);
}


Device::Device(cl_platform_id pid, cl_device_id did) :
	m_PlatformID(pid),
	m_DeviceID(did)
{}


string Device::GetDeviceType()
{
	shared_ptr<void> data;
	size_t size;

	this->QueueDeviceParamData(CL_DEVICE_TYPE, data, size);
	TEST_GIVEN_EXPRESSION_AND_THROW_CLPP_EXCEPTION(size != sizeof(cl_device_type), "设备类型返回值尺寸和类型尺寸不符合");

	cl_device_type * type = (cl_device_type *)data.get();
	if (*type == CL_DEVICE_TYPE_DEFAULT)
	{
		return "Default";
	}
	else if (*type == CL_DEVICE_TYPE_CPU)
	{
		return "CPU";
	}
	else if (*type == CL_DEVICE_TYPE_GPU)
	{
		return "GPU";
	}
	else if (*type == CL_DEVICE_TYPE_ACCELERATOR)
	{
		return "ACCELERATOR";
	}
	else
	{
		return "ALL";
	}
}

cl_uint Device::GetMaxComputeUnits()
{
	shared_ptr<void> data;
	size_t size;

	this->QueueDeviceParamData(CL_DEVICE_TYPE, data, size);

	return *(cl_uint *)data.get();
}

cl_uint Device::GetMaxWorkItemDimesions()
{
	shared_ptr<void> data;
	size_t size;

	this->QueueDeviceParamData(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, data, size);

	return *(cl_uint *)data.get();
}

cl_platform_id Device::GetPlatformID()
{
	return this->m_PlatformID;
}

cl_device_id Device::GetDeviceID()
{
	return this->m_DeviceID;
}

void Device::QueueDeviceParamData(cl_device_info name, shared_ptr<void> & data, size_t & size)
{
	cl_int error;

	error = ::clGetDeviceInfo(
		this->m_DeviceID,
		name,
		0,
		nullptr,
		&size);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "查询设备参数失败");
	TEST_GIVEN_EXPRESSION_AND_THROW_CLPP_EXCEPTION(size < 1, "设备参数尺寸为0");

	data.reset((void *)new char[size]);

	error = ::clGetDeviceInfo(
		this->m_DeviceID,
		name,
		size,
		data.get(),
		nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(error, "查询设备参数失败");
}

NS_CLPP_END