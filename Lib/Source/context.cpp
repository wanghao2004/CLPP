#pragma once

#include "../Include/context.h"

using namespace std;

NS_CLPP_BEGIN

Context * Context::BuildContext(
	cl_platform_id pid,
	cl_device_id did,
	int deviceCount)
{
	cl_int errCode;
	cl_context context;

	cl_context_properties context_prop[] =
	{
		CL_CONTEXT_PLATFORM, (cl_context_properties)pid, 0
	};

	Context * cpContext = new Context();
	if (cpContext == nullptr)
	{
		return nullptr;
	}

	context = ::clCreateContext(
		context_prop,
		deviceCount,
		&did,
		OnContextMessage,
		(void *)cpContext,
		&errCode);
	TEST_AND_THROW_CLPP_EXCEPTION(errCode, "创建上下文失败");

	*cpContext = context;

	cpContext->m_CommandQueue = CommandQueue::BuildCommandQueue(cpContext);

	return cpContext;
}

Context * Context::BuildFirstGPUContext()
{
	auto ids = clpp::Platform::EnumAllPlatformID();

	for (auto it_pid = ids.begin(); it_pid != ids.end(); it_pid++)
	{
		auto platform = clpp::Platform::BuildPlatform(*it_pid);

		auto dids = clpp::Device::EnumGPUDevcies(platform);
		for (auto it_did = dids.begin(); it_did != dids.end(); it_did++)
		{
			auto device = clpp::Device::Device::BuildDeviceByID(platform->GetPlatformID(), *it_did);
			if (device->GetDeviceType() == "GPU")
			{
				return clpp::Context::BuildContext(
					device->GetPlatformID(),
					device->GetDeviceID(),
					dids.size());
			}
		}
	}
}

Context::Context() : 
	m_ContextObject(nullptr)
{}

Context::Context(cl_context context) :
	m_ContextObject(context)
{}

Context::~Context()
{
	m_ContextObject = nullptr;

	cout << "delete context" << endl;
}

Context & Context::operator=(cl_context context)
{
	if (this->m_ContextObject == nullptr)
	{
		this->m_ContextObject = context;
	}

	return (*this);
}

void Context::AddRef()
{
	::clRetainContext(this->m_ContextObject);
}

void Context::Release()
{
	auto refCount = this->RefCount();

	::clReleaseContext(this->m_ContextObject);

	if (refCount == 1)
	{
		delete this;
	}
}

unsigned long Context::RefCount()
{
	shared_ptr<void> data;
	size_t size;

	this->QueryContextPropData(CL_CONTEXT_REFERENCE_COUNT, data, size);

	cl_uint refCount = *(cl_uint *)data.get();

	return (unsigned long)refCount;
}


vector<cl_device_id> Context::GetContextDevices()
{
	shared_ptr<void> data;
	size_t size;

	this->QueryContextPropData(CL_CONTEXT_DEVICES, data, size);

	cl_device_id * arrayDeviceIDs = (cl_device_id *)data.get();
	int deviceCount = (int)(size / sizeof(cl_device_id));

	vector<cl_device_id> vid;
	for (int n = 0; n < deviceCount; n++)
	{
		vid.push_back(arrayDeviceIDs[n]);
	}

	return vid;
}

Program * Context::CreateProgramWithSource(const char * SourceFile)
{
	return Program::BuildProgram(this, SourceFile);
}

void Context::ExcuteKernelByBlock(Kernel * kernel, cl_uint workDimension, size_t globalOffset, size_t globalSize, size_t localSize)
{
	cl_int errNum;

	errNum = ::clEnqueueNDRangeKernel(
		this->m_CommandQueue->GetRaw(),
		kernel->GetRaw(),
		workDimension,
		&globalOffset,
		&globalSize,
		&localSize,
		0, nullptr, nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "执行内核程序发生异常");
}

Buffer * Context::CreateBufferObjectWithData(void * data, size_t size, cl_mem_flags flags)
{
	return Buffer::BuildBufferObject(this, flags, data, size);
}

void Context::PullDataFromBufferByBlock(Buffer * buffer)
{
	cl_int errNum;

	errNum = ::clEnqueueReadBuffer(
		this->m_CommandQueue->GetRaw(),
		buffer->GetRaw(),
		CL_TRUE,
		0,
		buffer->GetHostBufferSize(),
		buffer->GetHostBufferPtr(),
		0,
		nullptr,
		nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "读取缓冲区失败");
}

void Context::PushDataToBufferByBlock(Buffer * buffer)
{
	cl_int errNum;

	errNum = ::clEnqueueWriteBuffer(
		this->m_CommandQueue->GetRaw(),
		buffer->GetRaw(),
		CL_TRUE,
		0,
		buffer->GetHostBufferSize(),
		buffer->GetHostBufferPtr(),
		0,
		nullptr,
		nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "写取缓冲区失败");
}

cl_context Context::GetRaw()
{
	return this->m_ContextObject;
}

void Context::staticOnContextMessage(const char * errorInfo, const void * clData, size_t size)
{
	if (this->m_ErrorCallback)
	{
		this->m_ErrorCallback(errorInfo, clData, size);
	}
}

void Context::SetErrorMessageCallback(MESSAGE_CALLBACK_TYPE cbErrorMessage)
{
	this->m_ErrorCallback = cbErrorMessage;
}

void Context::QueryContextPropData(cl_context_info name, shared_ptr<void> & data, size_t & size)
{
	cl_int err;

	err = ::clGetContextInfo(
		this->m_ContextObject,
		name,
		0,
		nullptr,
		&size);
	TEST_AND_THROW_CLPP_EXCEPTION(err, "查询上下文参数尺寸异常");

	data.reset(new char[size]);

	err = ::clGetContextInfo(
		this->m_ContextObject,
		name,
		size,
		data.get(),
		nullptr);
	TEST_AND_THROW_CLPP_EXCEPTION(err, "查询上下文参数异常");
}

NS_CLPP_END

void OnContextMessage(const char * errorInfo, const void * clData, size_t size, void * userData)
{
	clpp::Context * pContext = (clpp::Context *)(userData);

	pContext->staticOnContextMessage(errorInfo, clData, size);
}