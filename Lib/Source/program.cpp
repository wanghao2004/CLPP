#pragma once

#include <streambuf>
#include <iostream>

#include "../Include/program.h"

using namespace std;

NS_CLPP_BEGIN

Program * Program::BuildProgram(Context * context, const char * oclFile)
{
	cl_int errNum;

	ifstream srcFile(oclFile);
	TEST_GIVEN_EXPRESSION_AND_THROW_CLPP_EXCEPTION(!srcFile.is_open(), "打开OCL源文件异常");

	string srcProg(std::istreambuf_iterator<char>(srcFile), (std::istreambuf_iterator<char>()));

	const char * src = srcProg.c_str();
	size_t length = srcProg.length();

	cl_program rawProgram = ::clCreateProgramWithSource(
		context->GetRaw(),
		1,
		&src,
		&length,
		&errNum);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "利用源码创建程序对象时发生错误");

	auto vecDev = context->GetContextDevices();
	cl_uint deviceNum = (cl_uint)vecDev.size();
	unique_ptr<cl_device_id> deviceArray(new cl_device_id[deviceNum]);

	for (int i = 0; i < deviceNum; i++)
	{
		deviceArray.get()[i] = vecDev[i];
	}

	stringbuf buf;
	ostream logStream(&buf);
	errNum = ::clBuildProgram(rawProgram, deviceNum, deviceArray.get(), nullptr, nullptr, nullptr);
	if (errNum != CL_SUCCESS)
	{
		for (int dn = 0; dn < deviceNum; dn++)
		{
			cl_device_id curDeviceID = deviceArray.get()[dn];

			cl_int error2;
			size_t logSize;

			error2 = ::clGetProgramBuildInfo(rawProgram, curDeviceID, CL_PROGRAM_BUILD_LOG, 0, nullptr, &logSize);
			if (error2 != CL_SUCCESS || logSize < 0)
			{
				logStream << "在设备<" << curDeviceID <<">读取 CL_PROGRAM_BUILD_LOG 的尺寸失败" << endl;
				continue;
			}

			unique_ptr<char> logContent(new char[logSize]);

			error2 = ::clGetProgramBuildInfo(rawProgram, curDeviceID, CL_PROGRAM_BUILD_LOG, logSize, logContent.get(), nullptr);
			if (error2 != CL_SUCCESS)
			{
				logStream << "在设备<" << curDeviceID << ">读取 CL_PROGRAM_BUILD_LOG 失败" << endl;
				continue;
			}

			logStream << "设备<" << curDeviceID << ">的编译日志：" << endl;
			logStream << logContent.get() << endl << endl;
		}
	}
	TEST_AND_THROW_CLPP_BUILD_EXCEPTION(errNum, "编译链接程序对象时发生错误", buf.str());
	
	return new Program(rawProgram);
}


Program::Program(cl_program program) :
	m_Program(program)
{
}

Program::~Program()
{
	this->m_Program = nullptr;

	cout << "delete program" << endl;
}


void Program::AddRef()
{
	::clRetainProgram(this->m_Program);
}

void Program::Release()
{
	auto refCount = this->RefCount();
	::clReleaseProgram(this->m_Program);
	if (refCount == 1)
	{
		delete this;
	}
}

unsigned long Program::RefCount()
{
	shared_ptr<void> data;
	size_t size;

	this->QueryProgramParamData(CL_PROGRAM_REFERENCE_COUNT, data, size);

	cl_uint refcount = *(cl_uint *)data.get();

	return (unsigned long)refcount;
}

Kernel * Program::CreateKernel(const char * kernelName)
{
	cl_int errNum;

	cl_kernel kernel = ::clCreateKernel(this->m_Program, kernelName, &errNum);
	TEST_AND_THROW_CLPP_EXCEPTION(errNum, "创建内核对象失败");

	return new Kernel(kernel);
}

void Program::QueryProgramParamData(cl_profiling_info name, shared_ptr<void> & data, size_t & size)
{
	::clGetProgramInfo(
		this->m_Program,
		name,
		0,
		nullptr,
		&size);

	data.reset(new char[size]);

	::clGetProgramInfo(
		this->m_Program,
		name,
		size,
		data.get(),
		nullptr);
}

NS_CLPP_END