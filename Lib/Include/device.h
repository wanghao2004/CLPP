#pragma once

#include <vector>
#include <memory>

#include "base.h"
#include "platform.h"
#include "exception.h"

using namespace std;

NS_CLPP_BEGIN

class Device
{
public:
	static vector<cl_device_id> EnumGPUDevcies(const shared_ptr<Platform> & platform);

	static shared_ptr<Device> BuildDeviceByID(cl_platform_id pid, cl_device_id did);

public:
	Device(cl_platform_id pid, cl_device_id did);

public:
	string GetDeviceType();

	cl_uint GetMaxComputeUnits();

	cl_uint GetMaxWorkItemDimesions();

	cl_platform_id GetPlatformID();

	cl_device_id GetDeviceID();

private:
	void QueueDeviceParamData(cl_device_info name, shared_ptr<void> & data, size_t & size);

private:
	cl_platform_id m_PlatformID;
	cl_device_id  m_DeviceID;
};

NS_CLPP_END