#pragma once

#include <vector>
#include <memory>
#include <string>

#include "base.h"

using namespace std;

NS_CLPP_BEGIN

class Platform
{
public:
	static vector<cl_platform_id> EnumAllPlatformID();

	static shared_ptr<Platform> BuildPlatform(cl_platform_id platformID);

public:
	Platform(cl_platform_id platformID);

public:
	string GetProfile();

	string GetVersion();

	string GetName();

	string GetVendor();

	string GetExtension();

	cl_platform_id GetPlatformID();

private:
	string QueryPlatformInfo(cl_platform_info name);

private:
	cl_platform_id m_PlatformID;
};

NS_CLPP_END