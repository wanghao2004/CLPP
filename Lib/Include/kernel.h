#pragma once

#include <memory>

#include "base.h"
#include "exception.h"
#include "buffer.h"

using namespace std;

NS_CLPP_BEGIN

class Buffer;
class Kernel : public ICLObject
{
public:
	Kernel() = delete;

	Kernel(cl_kernel kernel);

	~Kernel();

public:
	virtual void AddRef();

	virtual void Release();

	virtual unsigned long RefCount();

public:
	template<class T>
	void SetBasicTypeArgument(int argIndex, T * argValue);

	void SetBufferArgument(int argIndex, Buffer * mem);

	cl_kernel GetRaw();

private:
	void QueryKernelObjectData(cl_kernel_info name, shared_ptr<void> & data, size_t & size);

private:
	cl_kernel m_KernelObject;
};

NS_CLPP_END