#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include "base.h"
#include "exception.h"
#include "context.h"
#include "kernel.h"

using namespace std;

NS_CLPP_BEGIN

class Kernel;
class Context;
class Program : public ICLObject
{
public:
	static Program * BuildProgram(Context * context, const char * oclFile);

public:
	Program(cl_program program);

	virtual ~Program();

public:
	virtual void AddRef();

	virtual void Release();

	virtual unsigned long RefCount();

public:
	Kernel * CreateKernel(const char * kernelName);

private:
	void QueryProgramParamData(cl_profiling_info name, shared_ptr<void> & data, size_t & size);

private:
	cl_program m_Program;
};

NS_CLPP_END