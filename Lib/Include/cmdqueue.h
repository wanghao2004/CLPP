#pragma once

#include <memory>

#include "base.h"
#include "exception.h"
#include "context.h"

using namespace std;

NS_CLPP_BEGIN

class CommandQueue : ICLObject
{
public:
	static CommandQueue * BuildCommandQueue(Context * context);
	
public:
	CommandQueue() = delete;

	CommandQueue(cl_command_queue cmdQueueObject);

	~CommandQueue();

public:
	virtual void AddRef();

	virtual void Release();

	virtual unsigned long RefCount();

public:
	cl_command_queue GetRaw();

private:
	void QueueCommandQueueParamData(cl_command_queue_info cmdQueueInfo, shared_ptr<void> & data, size_t & size);

private:
	cl_command_queue m_CommandQueueObject;
};

NS_CLPP_END