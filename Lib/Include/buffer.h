#pragma once

#include <memory>

#include "base.h"
#include "exception.h"
#include "context.h"

using namespace std;

NS_CLPP_BEGIN

class Context;
class Buffer : ICLObject
{
public:
	static Buffer * BuildBufferObject(Context * context, cl_mem_flags flags, void * data, size_t size);

public:
	Buffer() = delete;

	Buffer(cl_mem clBufferObj, shared_ptr<void> data, size_t size, cl_mem_flags flags);

	~Buffer();

public:
	virtual void AddRef();

	virtual void Release();

	virtual unsigned long RefCount();

public:
	void * GetHostBufferPtr();

	size_t GetHostBufferSize();

	cl_mem_flags GetBufferFlags();

	cl_mem GetRaw();

private:
	void QueryBufferParamData(cl_mem_info name, shared_ptr<void> & data, size_t & size);

private:
	cl_mem m_BufferObject;

	shared_ptr<void> m_Data;

	size_t m_Size;

	cl_mem_flags m_Flags;
};


NS_CLPP_END