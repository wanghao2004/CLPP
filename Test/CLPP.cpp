// CLPP.cpp : 定义控制台应用程序的入口点。
//

#include <iostream>
#include <algorithm>

#include "../Lib/Include/clpp.h"

using namespace std;


void PrintPlatformInfo();

void ExcuteKernel();

int main()
{
	// PrintPlatformInfo();

	ExcuteKernel();

    return 0;
}

void PrintPlatformInfo()
{
	try
	{
		cout << "PrintPlatformInfo :" << endl;

		auto ids = clpp::Platform::EnumAllPlatformID();

		for_each(ids.begin(), ids.end(), [](const cl_platform_id & id)
		{
			cout << "\t Platform[" << id << "]:" << endl;

			auto platform = clpp::Platform::BuildPlatform(id);
			cout << "\t\t Name : " << platform->GetName() << endl;
			cout << "\t\t Version : " << platform->GetVersion() << endl;
			cout << "\t\t Vendor : " << platform->GetVendor() << endl;
			cout << "\t\t Profile : " << platform->GetProfile() << endl;
			cout << "\t\t Extension : " << platform->GetExtension() << endl;

			auto dids = clpp::Device::EnumGPUDevcies(platform);
			for_each(dids.begin(), dids.end(), [&platform](const cl_device_id & did)
			{
				cout << "\t\t Devices : " << did << endl;

				auto device = clpp::Device::Device::BuildDeviceByID(platform->GetPlatformID(), did);
				cout << "\t\t\t Type : " << device->GetDeviceType() << endl;
				cout << "\t\t\t MaxComputeUnits : " << device->GetMaxComputeUnits() << endl;
				cout << "\t\t\t MaxWorkItemDimensions : " << device->GetMaxWorkItemDimesions() << endl;
			});
		});

		cout << endl << endl;
	}
	catch (clpp::Exception e)
	{
		cout << "@" << e.GetFileName() << " " << e.GetLine() << " : " << e.what() << endl;
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
	catch (...)
	{
		cout << "捕捉到未知异常" << endl;
	}
}

void ExcuteKernel()
{
	try
	{
		clpp::CLObjectGuard<clpp::Context> clContext = clpp::Context::BuildFirstGPUContext();
		
		cout << "Context RefCount = " << clContext->RefCount() << endl;

		cout << "GetContextDevices : " << endl;
		auto devs = clContext->GetContextDevices();
		for_each(devs.begin(), devs.end(), [](const cl_device_id & did) 
		{
			cout << "\t DeviceID = " << did << endl;
		});

		clContext->SetErrorMessageCallback([](const char * errMsg, const void * clErrData, size_t size) 
		{
			cout << "\t Context error happend : " << errMsg << endl;
		});

		clpp::CLObjectGuard<clpp::Program> clProgram = clContext->CreateProgramWithSource("CL/testOCL.cl");

		cout << "Program RefCount = " << clProgram->RefCount() << endl;

		clpp::CLObjectGuard<clpp::Kernel> clKernel = clProgram->CreateKernel("scalar_add");

		cout << "Kernel RefCount = " << clKernel->RefCount() << endl;

		const int N = 10;
		shared_ptr<void> A(new cl_float[N]);
		shared_ptr<void> B(new cl_float[N]);
		shared_ptr<void> C(new cl_float[N]);

		for (int n = 0; n < N; n++)
		{
			((cl_float *)A.get())[n] = n + 1;
			((cl_float *)B.get())[n] = n + 1;
		}

		clpp::CLObjectGuard<clpp::Buffer> bA = clContext->CreateBufferObjectWithData(A.get(), sizeof(cl_float) * N, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
		clpp::CLObjectGuard<clpp::Buffer> bB = clContext->CreateBufferObjectWithData(B.get(), sizeof(cl_float) * N, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
		clpp::CLObjectGuard<clpp::Buffer> bC = clContext->CreateBufferObjectWithData(C.get(), sizeof(cl_float) * N, CL_MEM_WRITE_ONLY);

		clKernel->SetBufferArgument(0, bA.get());
		clKernel->SetBufferArgument(1, bB.get());
		clKernel->SetBufferArgument(2, bC.get());

		size_t goffset = 0;
		size_t gsize = N;
		size_t lsize = 1;
		clContext->ExcuteKernelByBlock(clKernel.get(), 1, goffset, gsize, lsize);

		clContext->PullDataFromBufferByBlock(bC.get());

		for (int n = 0; n < N; n++)
		{
			cout << "C(" << n << ") = " << ((cl_float *)bC->GetHostBufferPtr())[n] << endl;
		}
	}
	catch (clpp::BuildException be)
	{
		cout << "@" << be.GetFileName() << " " << be.GetLine() << " : " << be.what() << "  ErrCode =" << be.GetCLErrorNumber() << endl;
		cout << "Build LOG:" << endl << be.GetBuildLog() << endl << endl;
	}
	catch (clpp::Exception e)
	{
		cout << "@" << e.GetFileName() << " " << e.GetLine() << " : " << e.what() << "  ErrCode =" << e.GetCLErrorNumber() << endl;
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
	catch (...)
	{
		cout << "捕捉到未知异常" << endl;
	}
}